﻿module CardToolsTests

open Xunit
open FsUnit.Xunit
open Common
open CardTools

[<Fact>]
let ``getSecondRoundBoards works correctly`` () =
    enumerateCards |> should equal [
        Card(Ace, Spades)
        Card(Ace, Hearts)
        Card(King, Spades)
        Card(King, Hearts)
        Card(Queen, Spades)
        Card(Queen, Hearts)
    ] 