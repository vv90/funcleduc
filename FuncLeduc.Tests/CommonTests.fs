﻿module CommonTests

open Xunit
open FsUnit.Xunit
open Common

type TestUnion = First | Second | Third

[<Fact>]
let ``allUnionCases returns union cases correctly`` () =
    allUnionCases<TestUnion>() |> should equal [|First; Second; Third|]

[<Fact>]
let ``countChildrenDfs counts children correctly`` () =
    let root = 
        {
            Info = {
                Type = None;
                Terminal = false;
                CurrentPlayer = P1;
                Street = Preflop;
                Board = None;
                Bets = (100, 100);
                Pot = 100;
            };
            Ranges = Some (Array.zeroCreate 0);
            Strategy = Array2D.zeroCreate 0 0;
            RangesAbsolute = Array2D.zeroCreate 0 0;
            Children = [|
                {
                    Info = {
                        Type = Some TerminalFold;
                        Terminal = true;
                        CurrentPlayer = P2;
                        Street = Preflop;
                        Board = None;
                        Bets = (100, 100);
                        Pot = 100;
                    };
                    Ranges = Some (Array.zeroCreate 0);
                    Strategy = Array2D.zeroCreate 0 0;
                    RangesAbsolute = Array2D.zeroCreate 0 0;
                    Children = [||];
                }
                {
                    Info = {
                        Type = Some Check;
                        Terminal = false;
                        CurrentPlayer = P2;
                        Street = Preflop;
                        Board = None;
                        Bets = (100, 100);
                        Pot = 100;
                    };
                    Ranges = Some (Array.zeroCreate 0);
                    Strategy = Array2D.zeroCreate 0 0;
                    RangesAbsolute = Array2D.zeroCreate 0 0;
                    Children = [|
                        {
                            Info = {
                                Type = Some TerminalFold;
                                Terminal = true;
                                Street = Preflop;
                                Board = None;
                                CurrentPlayer = P1;
                                Bets = (100, 100);
                                Pot = 100;
                            };
                            Ranges = Some (Array.zeroCreate 0);
                            Strategy = Array2D.zeroCreate 0 0;
                            RangesAbsolute = Array2D.zeroCreate 0 0;
                            Children = [||];
                        }
                        {
                            Info = {
                                Type = Some NodeType.Chance;
                                Terminal = false;
                                Street = Preflop;
                                Board = None;
                                CurrentPlayer = Chance;
                                Bets = (100, 100);
                                Pot = 100;
                            };
                            Ranges = Some (Array.zeroCreate 0);
                            Strategy = Array2D.zeroCreate 0 0;
                            RangesAbsolute = Array2D.zeroCreate 0 0;
                            Children = [||];
                        }
                        {
                            Info = {
                                Type = None;
                                Terminal = false;
                                Street = Preflop;
                                Board = None;
                                CurrentPlayer = P1;
                                Bets = (100, 300);
                                Pot = 100;
                            };
                            Ranges = Some (Array.zeroCreate 0);
                            Strategy = Array2D.zeroCreate 0 0;
                            RangesAbsolute = Array2D.zeroCreate 0 0;
                            Children = [||];
                        }
                        {
                            Info = {
                                Type = None;
                                Terminal = false;
                                Street = Preflop;
                                Board = None;
                                CurrentPlayer = P1;
                                Bets = (100, 500);
                                Pot = 100;
                            };
                            Ranges = Some (Array.zeroCreate 0);
                            Strategy = Array2D.zeroCreate 0 0;
                            RangesAbsolute = Array2D.zeroCreate 0 0;
                            Children = [||];
                        }
                        {
                            Info = {
                                Type = None;
                                Terminal = false;
                                Street = Preflop;
                                Board = None;
                                CurrentPlayer = P1;
                                Bets = (100, 1200);
                                Pot = 100;
                            };
                            Ranges = Some (Array.zeroCreate 0);
                            Strategy = Array2D.zeroCreate 0 0;
                            RangesAbsolute = Array2D.zeroCreate 0 0;
                            Children = [||];
                        }
                    |]
                }
                {
                    Info = {
                        Type = None;
                        Terminal = false;
                        CurrentPlayer = P2;
                        Street = Preflop;
                        Board = None;
                        Bets = (300, 100);
                        Pot = 100;
                    };
                    Ranges = Some (Array.zeroCreate 0);
                    Strategy = Array2D.zeroCreate 0 0;
                    RangesAbsolute = Array2D.zeroCreate 0 0;
                    Children = [||];
                }
                {
                    Info = {
                        Type = None;
                        Terminal = false;
                        CurrentPlayer = P2;
                        Street = Preflop;
                        Board = None;
                        Bets = (500, 100);
                        Pot = 100;
                    };
                    Ranges = Some (Array.zeroCreate 0);
                    Strategy = Array2D.zeroCreate 0 0;
                    RangesAbsolute = Array2D.zeroCreate 0 0;
                    Children = [||];
                }
                {
                    Info = {
                        Type = None;
                        Terminal = false;
                        CurrentPlayer = P2;
                        Street = Preflop;
                        Board = None;
                        Bets = (1200, 100);
                        Pot = 100;
                    };
                    Ranges = Some (Array.zeroCreate 0);
                    Strategy = Array2D.zeroCreate 0 0;
                    RangesAbsolute = Array2D.zeroCreate 0 0;
                    Children = [||];
                }
            |]
        }

    countChildrenDfs root |> should equal 10