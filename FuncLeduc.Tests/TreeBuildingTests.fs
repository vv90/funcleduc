module TreeBuildingTests

open Xunit
open FsUnit.Xunit
open Common
open TreeBuilder

[<Fact>]
let ``getChildrenNodes expands root node correctly`` () =
    let node = {
        Type = None;
        Terminal = false;
        CurrentPlayer = P1;
        Street = Preflop;
        Board = None;
        Bets = (100, 100);
        Pot = 100;
    }

    let children = getChildrenNodes node

    children |> should equal [
        {
            Type = Some TerminalFold;
            Terminal = true;
            CurrentPlayer = P2;
            Street = Preflop;
            Board = None;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = Some Check;
            Terminal = false;
            CurrentPlayer = P2;
            Street = Preflop;
            Board = None;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = None;
            Terminal = false;
            CurrentPlayer = P2;
            Street = Preflop;
            Board = None;
            Bets = (300, 100);
            Pot = 100;
        }
        {
            Type = None;
            Terminal = false;
            CurrentPlayer = P2;
            Street = Preflop;
            Board = None;
            Bets = (500, 100);
            Pot = 100;
        }
        {
            Type = None;
            Terminal = false;
            CurrentPlayer = P2;
            Street = Preflop;
            Board = None;
            Bets = (1200, 100);
            Pot = 100;
        }
    ]

[<Fact>]
let ``getChildrenNodes expands check node correctly`` () =
    let node = {
        Type = Some Check;
        Terminal = false;
        CurrentPlayer = P2;
        Street = Preflop;
        Board = None;
        Bets = (100, 100);
        Pot = 100;
    }

    getChildrenNodes node |> should equal [
        {
            Type = Some TerminalFold;
            Terminal = true;
            Street = Preflop;
            Board = None;
            CurrentPlayer = P1;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = Some NodeType.Chance;
            Terminal = false;
            Street = Preflop;
            Board = None;
            CurrentPlayer = Chance;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = None;
            Terminal = false;
            Street = Preflop;
            Board = None;
            CurrentPlayer = P1;
            Bets = (100, 300);
            Pot = 100;
        }
        {
            Type = None;
            Terminal = false;
            Street = Preflop;
            Board = None;
            CurrentPlayer = P1;
            Bets = (100, 500);
            Pot = 100;
        }
        {
            Type = None;
            Terminal = false;
            Street = Preflop;
            Board = None;
            CurrentPlayer = P1;
            Bets = (100, 1200);
            Pot = 100;
        }
    ]
       
[<Fact>]
let ``getChildrenNodes expands chance node correctly`` () =
    let node = {
        Type = Some NodeType.Chance;
        Terminal = false;
        Street = Preflop;
        Board = None;
        CurrentPlayer = Chance;
        Bets = (100, 100);
        Pot = 100;
    }

    getChildrenNodes node |> should equal [
        {
            Type = Some InnerNode;
            Terminal = false;
            Street = Flop;
            Board = Some (Card(Ace, Spades));
            CurrentPlayer = P1;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = Some InnerNode;
            Terminal = false;
            Street = Flop;
            Board = Some (Card(Ace, Hearts));
            CurrentPlayer = P1;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = Some InnerNode;
            Terminal = false;
            Street = Flop;
            Board = Some (Card(King, Spades));
            CurrentPlayer = P1;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = Some InnerNode;
            Terminal = false;
            Street = Flop;
            Board = Some (Card(King, Hearts));
            CurrentPlayer = P1;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = Some InnerNode;
            Terminal = false;
            Street = Flop;
            Board = Some (Card(Queen, Spades));
            CurrentPlayer = P1;
            Bets = (100, 100);
            Pot = 100;
        }
        {
            Type = Some InnerNode;
            Terminal = false;
            Street = Flop;
            Board = Some (Card(Queen, Hearts));
            CurrentPlayer = P1;
            Bets = (100, 100);
            Pot = 100;
        }
    ]