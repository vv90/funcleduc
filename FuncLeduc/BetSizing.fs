﻿module BetSizing

open Common

let makeBets (p1: Player * int) (p2: Player * int): (int * int) = 
    assert (fst p1 <> Chance && fst p2 <> Chance && fst p1 <> fst p2)
    if fst p1 = P1 then (snd p1, snd p2)
    else (snd p2, snd p1)


let getPossibleBets parentNode: (int * int) list =
    assert (parentNode.CurrentPlayer = P1 || parentNode.CurrentPlayer = P2)

    let opponent = nextPlayer parentNode.CurrentPlayer
    let oppBet = playerBet parentNode.Bets opponent
    let curBet = playerBet parentNode.Bets parentNode.CurrentPlayer;

    assert (playerBet parentNode.Bets parentNode.CurrentPlayer <= playerBet parentNode.Bets opponent)

    let maxRaiseSize = stack - oppBet
    let minRaiseSize =  min (max (oppBet - curBet) ante) maxRaiseSize

    if minRaiseSize = 0 then []
    elif minRaiseSize = maxRaiseSize then 
        [(oppBet, oppBet); makeBets (opponent, oppBet) (parentNode.CurrentPlayer, oppBet + minRaiseSize)]
    else
        let pot = oppBet * 2
        (possibleBets 
        |> List.map (fun bet -> int ((float pot) * bet))
        |> List.filter (fun bet -> bet >= minRaiseSize && bet < maxRaiseSize) 
        |> List.map (fun bet -> makeBets (opponent, oppBet) (parentNode.CurrentPlayer, oppBet + bet))) 
        @ [makeBets (opponent, oppBet) (parentNode.CurrentPlayer, oppBet + maxRaiseSize)]
