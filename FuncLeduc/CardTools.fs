﻿module CardTools

open Common

let getPossibleHandIndexes (board:Card option) =
    match board with
    | None -> cards |> Seq.map (fun c -> true) |> Array.ofSeq
    | Some card -> cards |> Seq.map (fun c -> not (c = card)) |> Array.ofSeq
