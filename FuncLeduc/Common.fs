﻿module Common

open Microsoft.FSharp.Reflection

let allUnionCases<'T>() =
    FSharpType.GetUnionCases(typeof<'T>)
    |> Array.map (fun case -> FSharpValue.MakeUnion(case, [||]) :?> 'T)

type Suit = Spades | Hearts
type Rank = Ace | King | Queen
type Card = Rank * Suit

type Amount = Amount of float


type PlayerAction = Fold | Call of Amount | Raise of Amount

type Street = Preflop | Flop
type NodeType = TerminalFold | TerminalCall | Check | Chance | InnerNode
type Player = Chance | P1 | P2

type NodeInfo = {
    Type: NodeType option;
    Terminal: bool;
    CurrentPlayer: Player;
    Street: Street;
    Board: Card option;
    Pot: int;
    Bets: int * int; 
}

type Range = {
    P1: Map<Card, float>
}

type Node = {
    Info: NodeInfo;
    Children: Node[];
    Ranges: float[] option;
    RangesAbsolute: float[,];
    Strategy: float[,];
}

type LuaNode = {
    node_type: int option;
    terminal: bool option;
    street: int;
    current_player: int;
    bets: int list;
    pot: int;
    children: LuaNode list;
    actions: int[];
    strategy: float[,]
}

let stack = 1200
let ante = 100
let possibleBets = [1.0; 2.0]
let cardCount = allUnionCases<Suit>().Length * allUnionCases<Rank>().Length
let playerCount = 2


let cards = [|
    Card(Ace, Spades)
    Card(Ace, Hearts)
    Card(King, Spades)
    Card(King, Hearts)
    Card(Queen, Spades)
    Card(Queen, Hearts)
|]

let cardIndexMap = cards |> Array.mapi (fun i c -> (c, i)) |> Map.ofArray

let cardToIndex c =
    cardIndexMap.[c]

let indexToCard i =
    cards.[i]

let playerToIndex (player: Player) =
    match player with 
    | Chance -> -1
    | P1 -> 0
    | P2 -> 1

let nextPlayer player =
    match player with
    | Chance -> P1
    | P1 -> P2
    | P2 -> P1

let maxBet tup = 
    max (fst tup) (snd tup)



let playerBet (tup: int * int) (player: Player): int =
    assert (player = P1 || player = P2)

    match player with
    | P1 -> (fst tup)
    | P2 -> (snd tup)
    | _ -> invalidArg "player" "invalid player"

let enumerateCards: Card list =
    [for r in allUnionCases<Rank>() do
        for s in allUnionCases<Suit>() do
            yield Card(r, s)]

let getUniformRange: Map<Card, float> =
    enumerateCards |> List.map (fun card -> (card, 1.0/(float)cardCount)) |> Map.ofList

    
let nextStreet street =
    Flop

let rec countChildrenDfs (node: Node)=
    node.Children.Length + Array.fold (fun acc child -> acc + countChildrenDfs child) 0 node.Children
 
let rec countLuaChildrenDfs (node: LuaNode)=
    node.children.Length + List.fold (fun acc child -> acc + countLuaChildrenDfs child) 0 node.children
    