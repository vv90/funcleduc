﻿// Learn more about F# at http://fsharp.org

open System
open Common
open BetSizing
open TreeBuilder
open TreeValues
open Json
open System.IO





let isTerminal state =
    false

let allPlaying state = 
    true

let getWinner state = 
    Some 0

let getCurrentPlayer state =
    0

let otherPlayer state player =
    1

let getUtility state = 
    if not (isTerminal state) then None
    else 
        Some 100

let getTerminalUtility state cards = 
    if not (isTerminal state) then None
    elif not (allPlaying state) then getUtility state
    else 
        // check that both hands exsist
        // if (cards.Length < 2 || cards[0].IsEmpty() || cards[1].IsEmpty()) throw Exception ("opponent hand is empty")
        match getWinner state with
        | None -> Some 0
        | _ -> getUtility state


let buildTree() =
    let rootNodeInfo = {
        Type = Some NodeType.InnerNode;
        Terminal = false;
        CurrentPlayer = P1;
        Street = Preflop;
        Board = None;
        Bets = (ante, ante);
        Pot = 2 * ante;
    }

    let tree = buildTree rootNodeInfo
    let nodesCount = 1 + countChildrenDfs tree
    let json = serialize tree

    File.WriteAllText("tree.json", json)
    printf "%d nodes" nodesCount

    tree


let readLuaTree() =
    let data = File.ReadAllText("simple_tree.json")
    let tree = 
        match deserialize<LuaNode> data with
        | Ok t -> Some t
        | Error ex -> None

    if tree.IsSome then
        let nodesCount = 1 + countLuaChildrenDfs tree.Value
        printf "\n%d nodes" nodesCount

    0

[<EntryPoint>]
let main argv =

    // let res = readLuaTree()
    let res = buildTree()

    let startingRanges = Array2D.init playerCount cardCount (fun i j -> 1.0/(float)cardCount)
    fillRangesDfs res startingRanges


    0 // return an integer exit code