﻿module TreeBuilder

open Common
open BetSizing
open CardTools

let getUniformStrategy (children: Node[]) (nodeInfo: NodeInfo): float[,] =
    if nodeInfo.Terminal then Array2D.zeroCreate 0 0
    else Array2D.init children.Length cardCount (fun i j -> 1.0/(float)children.Length)

let getChanceStrategy (children: Node[]) (nodeInfo: NodeInfo): float[,] =
    assert (not nodeInfo.Terminal)
    assert (children.Length = cardCount)
    let possibleHandIndexes = Array.init children.Length (fun i -> children.[i].Info.Board |> getPossibleHandIndexes)
    Array2D.init children.Length cardCount (fun i j -> if possibleHandIndexes.[i].[j] then 1.0/((float)cardCount - 2.0) else 0.0)

let getStrategy (children: Node[]) (nodeInfo: NodeInfo): float[,] =
    match nodeInfo.CurrentPlayer with
    | Chance -> getChanceStrategy children nodeInfo
    | _ -> getUniformStrategy children nodeInfo

let getChildrenPlayerNode (parentNode: NodeInfo) : NodeInfo[] =
  
    let foldNode = {
        Type = Some TerminalFold; 
        Terminal = true;
        CurrentPlayer = (nextPlayer parentNode.CurrentPlayer)
        Street = parentNode.Street;
        Board = parentNode.Board;
        Bets = parentNode.Bets;
        Pot = parentNode.Pot;
    }

    let secondNode =
        if parentNode.CurrentPlayer = P1 && (fst parentNode.Bets = snd parentNode.Bets) then
            // check action
            {
                Type = Some NodeType.Check;
                Terminal = false;
                CurrentPlayer = (nextPlayer parentNode.CurrentPlayer);
                Street = parentNode.Street;
                Board = parentNode.Board;
                Bets = parentNode.Bets;
                Pot = parentNode.Pot;
            }
        elif (parentNode.Street = Preflop) && ((parentNode.CurrentPlayer = P2 && fst parentNode.Bets = snd parentNode.Bets) || (fst parentNode.Bets <> snd parentNode.Bets && maxBet parentNode.Bets < stack)) then
            // transition call
            let bets = (maxBet parentNode.Bets, maxBet parentNode.Bets)
            {
                Type = Some NodeType.Chance;
                Terminal = false;
                Street = parentNode.Street;
                Board = parentNode.Board;
                CurrentPlayer = Chance;
                Bets = bets;
                Pot = min (fst bets) (snd bets);
            }
        else 
            // terminal call
            let bets = (maxBet parentNode.Bets, maxBet parentNode.Bets)
            {
                Type = Some NodeType.TerminalCall;
                Terminal = true;
                CurrentPlayer = (nextPlayer parentNode.CurrentPlayer);
                Street = parentNode.Street;
                Board = parentNode.Board;
                Bets = bets;
                Pot = min (fst bets) (snd bets);
            }

    Array.concat [| 
        [|foldNode; secondNode|]
        [|for bet in getPossibleBets parentNode do 
            yield {
                Type = None;
                Terminal = false;
                CurrentPlayer = (nextPlayer parentNode.CurrentPlayer);
                Street = parentNode.Street;
                Board = parentNode.Board;
                Bets = bet;
                Pot = min (fst bet) (snd bet);
            }
        |]
    |]


let getChildrenChanceNode (parentNode: NodeInfo): NodeInfo[] =
    [|for c in enumerateCards do
        yield {
            parentNode with 
                Type = Some InnerNode;
                Street = nextStreet parentNode.Street; 
                Board = Some c; 
                CurrentPlayer = P1;
        }|]
            
            
let getChildrenNodes (parentNode: NodeInfo): NodeInfo[] =
    if parentNode.Terminal then 
        [||]
    elif parentNode.Type = Some NodeType.Chance then
        getChildrenChanceNode parentNode
    else
        getChildrenPlayerNode parentNode
           

let rec buildTree (nodeInfo: NodeInfo): Node = 
    let children = getChildrenNodes nodeInfo |> Array.map (fun n -> buildTree n)
    {
        Info = nodeInfo;
        Children = children;
        Ranges = None;
        Strategy = getStrategy children nodeInfo;
        RangesAbsolute = Array2D.zeroCreate playerCount cardCount
    }

