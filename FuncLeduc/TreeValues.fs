﻿module TreeValues

open Common
open CardTools

type ``[,,]``<'a> with
    member this.GetSlice(x: int, startY: int option, endY: int option, startZ: int option, endZ: int option) =
        let startY, endY = 
            Option.defaultValue (this.GetLowerBound 1) startY,
            Option.defaultValue (this.GetUpperBound 1) endY
        let startZ, endZ = 
            Option.defaultValue (this.GetLowerBound 2) startZ,
            Option.defaultValue (this.GetUpperBound 2) endZ
        let slice = Array2D.zeroCreate<'a> (this.GetLength 1) (this.GetLength 2)
        for y in startY..endY do
            for z in startZ..endZ do
               slice.[y-startY, z-startZ] <- this.[x,y,z]
        slice
    member this.GetSlice(startX: int option, endX: int option, y: int, startZ: int option, endZ: int option) =
        let startX, endX = 
            Option.defaultValue (this.GetLowerBound 1) startX,
            Option.defaultValue (this.GetUpperBound 1) endX
        let startZ, endZ = 
            Option.defaultValue (this.GetLowerBound 2) startZ,
            Option.defaultValue (this.GetUpperBound 2) endZ
        let slice = Array2D.zeroCreate<'a> (this.GetLength 1) (this.GetLength 2)
        for x in startX..endX do
            for z in startZ..endZ do
               slice.[x-startX, z-startZ] <- this.[x,y,z]
        slice
    member this.GetSlice(startX: int option, endX: int option, startY: int option, endY: int option, z: int) =
        let startX, endX = 
            Option.defaultValue (this.GetLowerBound 1) startX,
            Option.defaultValue (this.GetUpperBound 1) endX
        let startY, endY = 
            Option.defaultValue (this.GetLowerBound 2) startY,
            Option.defaultValue (this.GetUpperBound 2) endY
        let slice = Array2D.zeroCreate<'a> (this.GetLength 1) (this.GetLength 2)
        for x in startX..endX do
            for y in startY..endY do
               slice.[x-startX, y-startY] <- this.[x,y,z]
        slice

let uncurry3 f (a, b, c) = f a b c

let inverse (arr: seq<bool>): seq<bool> =
    arr |> Seq.map (fun b -> not b)

let cmul (arr1: seq<float>, arr2: seq<float>): seq<float> =
    arr1 |> Seq.zip arr2 |> Seq.map (fun (a1, a2) -> a1 * a2)

let cmul2d (arr1: float[,]) (arr2: float[,]): float[,] = 
    Array2D.init (Array2D.length1 arr1) (Array2D.length2 arr1) (fun i j -> arr1.[i, j] * arr2.[i, j])

let toFloat (arr: seq<bool>): seq<float> =
    arr |> Seq.map (fun a -> if a then 1.0 else 0.0)

let expand (dim: int) (arr: seq<float>): float[,] =
    array2D (Seq.init dim (fun i -> arr))


let flatten2d<'T> (arr: 'T[,]): seq<'T> =
    seq {
        for i in 0 .. arr.GetLength 0 - 1 do
            for j in 0 .. arr.GetLength 1 - 1 do
                yield arr.[i, j]
    }

let sum2dCol (arr: float[,]): seq<float> =
    seq {
        for i in 0 .. arr.GetLength 0 - 1 do
            yield arr.[i, *] |> Array.fold (fun s p -> p + s) 0.0
    }

let copy2d (arrTo: 'T[,]) (arrFrom: 'T[,]): unit =
    assert (Array2D.length1 arrTo = Array2D.length1 arrFrom && Array2D.length2 arrTo = Array2D.length2 arrFrom)
    for i in 0 .. Array2D.length1 arrTo - 1 do
        for j in 0 .. Array2D.length2 arrTo - 1 do
            arrTo.[i, j] <- arrFrom.[i, j]

let rec fillRangesDfs (node: Node) (rangesAbsolute: float[,]): unit =
    
    rangesAbsolute |> copy2d node.RangesAbsolute
    
    

    
    assert (rangesAbsolute |> flatten2d |> Seq.tryFind (fun p -> p < 0.0)).IsNone
    assert (rangesAbsolute |> flatten2d |> Seq.tryFind (fun p -> p > 1.0)).IsNone

    let handsMask = getPossibleHandIndexes node.Info.Board

    assert ((handsMask |> inverse |> toFloat |> expand (Array2D.length1 rangesAbsolute) |> cmul2d rangesAbsolute |> flatten2d |> Seq.sum) = 0.0)

    // rangesAbsolute
    // P1: [0.15; 0.15; 0.15; 0.15; 0.15; 0.15]
    // P2: [0.15; 0.15; 0.15; 0.15; 0.15; 0.15]

    let dimentions = (node.Children.Length, Array2D.length1 rangesAbsolute, Array2D.length2 rangesAbsolute)

    let childrenRangesAbsolute = 
        if node.Info.CurrentPlayer = Chance then 
            let columnSum = node.Strategy |>  sum2dCol
            assert (node.Strategy |> flatten2d |> Seq.tryFind (fun p -> p < 0.0)).IsNone
            assert (columnSum |> Seq.tryFind (fun p -> p > 1.001)).IsNone
            assert (columnSum |> Seq.tryFind (fun p -> p < 0.999)).IsNone

            uncurry3 Array3D.init dimentions (fun i j k -> rangesAbsolute.[j, k] * node.Strategy.[i, k])
        else 
            uncurry3 Array3D.init dimentions (fun i j k -> 
                if (playerToIndex node.Info.CurrentPlayer) = j then rangesAbsolute.[j, k] * node.Strategy.[i, k] 
                else rangesAbsolute.[j, k])

    for i in 0 .. node.Children.Length - 1 do
        fillRangesDfs (node.Children.[i]) (childrenRangesAbsolute.[i, *, *])
        